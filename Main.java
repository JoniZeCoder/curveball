package sample;

import javafx.animation.*;
import javafx.application.Application;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.Property;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

public class Main extends Application {
    static Rectangle pad = new Rectangle();
    static double WIDTH= 600;
    static double HEIGHT= 600;
    static double circleBaseRadius= 50;
    static Position bounds = new Position(WIDTH,HEIGHT,300);
    static double verzerrung = 0.6;

    @Override
    public void start(Stage primaryStage) throws Exception{
//        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        Pane root = new Pane();
        primaryStage.setScene(new Scene(root, WIDTH, HEIGHT));

        pad.setHeight(80);
        pad.setWidth(110);
        pad.setFill(Color.BLUE);

        root.getChildren().add(pad);
        root.addEventHandler(MouseEvent.MOUSE_MOVED, new mausHover());

        Timer t = new Timer();
        MovingObject mo = new MovingObject(100,100,0,1,1,1,100);
        root.getChildren().add(mo);
        mo.setFill(Color.GREEN);
        mo.setCenterX(50);
        mo.setCenterY(50);
        mo.setRadius(50);

        System.out.println(getHitPoint(new Position(50,0,0),45,135));

        PathTransition draw = new PathTransition();
        Path p = new Path();
        draw.setPath(p);
        Position p1 = new Position(50,0,0);
        System.out.println("p1 - " + p1);
        p.getElements().add(new MoveTo(p1.x,p1.y));
        Position p2 = getHitPoint(p1,45,90);
        System.out.println("p2 - " + p2);
        p.getElements().add(new LineTo(p2.x,p2.y));
        Position p3 = getHitPoint(p2,-45,90);
        System.out.println("p3 - " + p3);
        p.getElements().add(new LineTo(p3.x,p3.y));

        Rectangle rectParallel = new Rectangle(10,200,50, 50);
        rectParallel.setArcHeight(15);
        rectParallel.setArcWidth(15);
        rectParallel.setFill(Color.DARKBLUE);
        rectParallel.setTranslateX(50);
        rectParallel.setTranslateY(75);

        FadeTransition fadeTransition =
                new FadeTransition(Duration.millis(3000), rectParallel);
        fadeTransition.setFromValue(1.0f);
        fadeTransition.setToValue(0.3f);
        fadeTransition.setCycleCount(2);
        fadeTransition.setAutoReverse(true);
        TranslateTransition translateTransition =
                new TranslateTransition(Duration.millis(2000), rectParallel);
        translateTransition.setFromX(20);
        translateTransition.setToX(550);
        translateTransition.setCycleCount(2);
        translateTransition.setAutoReverse(true);
        RotateTransition rotateTransition =
                new RotateTransition(Duration.millis(3000), rectParallel);
        rotateTransition.setByAngle(180f);
        rotateTransition.setCycleCount(4);
        rotateTransition.setAutoReverse(true);
        ScaleTransition scaleTransition =
                new ScaleTransition(Duration.millis(2000), rectParallel);
        scaleTransition.setToX(2f);
        scaleTransition.setToY(2f);
        scaleTransition.setCycleCount(2);
        scaleTransition.setAutoReverse(true);

        ParallelTransition parallelTransition = new ParallelTransition();
        parallelTransition.getChildren().addAll(
                fadeTransition,
                translateTransition,
                rotateTransition,
                scaleTransition
        );
        parallelTransition.setCycleCount(Timeline.INDEFINITE);
        parallelTransition.play();

        root.getChildren().add(rectParallel);

        primaryStage.show();
        t.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                mo.move();
            }
        },1 ,50);
    }

    @Override
    public void stop() throws Exception {
        System.exit(0);
    }

    class mausHover implements EventHandler<MouseEvent> {
        @Override
        public void handle(MouseEvent event) {
            double x = event.getSceneX() - (pad.getWidth()/2);
            double y = event.getSceneY() - (pad.getHeight()/2);
            pad.setX(x);
            pad.setY(y);
        }
    }


    public static void main(String[] args) {
        launch(args);
    }

    public Position getHitPoint(Position startPoint, double angleHorizontal, double angleVertikal) {
        Position hitPoint = new Position();
        Position pointlinksrechts = calculateHitpoint(startPoint.x,100,500,angleHorizontal);
        Position pointobenunten = calculateHitpoint(startPoint.y,100,500,angleVertikal);
        System.out.println("pointlinksrechts: " + pointlinksrechts);
        System.out.println("pointobenunten: " + pointobenunten);
        if(pointlinksrechts.y < pointobenunten.y) {
            hitPoint.x = pointlinksrechts.x;
            hitPoint.y = calculateHitpoint(startPoint.y,100,pointlinksrechts.y, angleVertikal).x;
            hitPoint.z = pointlinksrechts.y;
        }else {
            hitPoint.y = pointobenunten.x;
            hitPoint.x = calculateHitpoint(startPoint.x, 100, pointobenunten.y, angleHorizontal).x;
            hitPoint.z = pointobenunten.y;
        }

        return hitPoint;
    }

    public Position calculateHitpoint(double a, double aMax, double yMax, double alpha) {
        double aRechnung;

        if(alpha > 90) {
            alpha = 180 - alpha;
            aRechnung = aMax - a;
            a = aMax;
        } else {
            aRechnung = a;
            a = 0;
        }

        double y;
        if((y = aRechnung/Math.tan(Math.toRadians(alpha))) >= yMax) {
            System.out.println("y = " + y);
            y = yMax;
            a = y *  Math.tan(Math.toRadians(alpha));
        }

        return new Position(a, y);
    }

    static Position transformPoint(Position p) {
        Position pVerzerrt = new Position();
        double verzerrung = (1-Main.verzerrung) * ((Main.bounds.z-p.z)/Main.bounds.z) + Main.verzerrung;
        pVerzerrt.x = Main.bounds.x * ((1-verzerrung) * 1/2) +(p.x * verzerrung);
        pVerzerrt.y = Main.bounds.y * ((1-verzerrung) * 1/2) +(p.y * verzerrung);
        pVerzerrt.z = 0;
        return pVerzerrt;
    }

    static double transformLength(double l, double z) {
        double verzerrung = Main.verzerrung * (z/Main.bounds.z);
        return l*verzerrung;
    }
}