package sample;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

class MovingObject extends Circle {
    Position vector;
    Position position3d;
    double radios;

    public MovingObject(double x,double y, double z, double speedX, double speedY, double speedZ, double circleRadios) {
        vector = new Position(speedX,speedY,speedZ);
        position3d = new Position(x,y,z);
        this.setFill(Color.RED);

        this.radios = circleRadios;
    }

    void move () {

    }
}