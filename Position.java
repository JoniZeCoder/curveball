package sample;


class Position {
    double x;
    double y;
    double z;

    public Position(){};

    public Position(double x, double y) {
        this.x = x;
        this.y = y;
        this.z = 0;
    }

    public Position(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public String toString() {
        return  ("x="+x+"; y="+y+"; z="+z+";");
    }
}